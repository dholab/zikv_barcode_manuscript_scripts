README FOR ZIKV BARCODE ANALYSIS PIPELINE

This pipeline is in three parts, intended to be run sequentially. No changes are required for Part 1, any changes to file names used in Part 2 and 3 are optional and will be changed only in the first cell of code in each notebook. Any time a notebook is run, it should be run by going to Cell > Run All. Running cells individually may cause unexpected errors. 


PART 1 

Purpose: Takes as input a folder of paired-end FASTQ files and outputs a file for each pair with a list of barcode sequences present as well as the number of each barcode.

Requirements:
1. Your paired-end FASTQ files must be named with "-R1.fastq.gz" and "-R2.fastq.gz" at the end of each file path.

2. All FASTQ files should be placed in the empty folder named "02-renamed_fastq_portal_names/"



PART 2

Purpose: Takes as input the files created in Part 1 and outputs a pivot table containing barcode sequences and counts for each across all samples provided in Part 1. 

Options: If desired, the output path (CSV_OUTPUT_PATH) can be changed. 
 


PART 3

Purpose: Takes as input the pivot table created in Part 2 and outputs a CSV file containing (1) all barcodes passing the percentage threshold determined based on the dataset provided, (2) percentage of each threshold-passing barcode for each sample, (3) percentage of all other non-threshold-passing barcodes for each sample, and (4) the sum of all barcode counts present for each sample.

Requirements: 
1. PIVOT_PATH must match CSV_OUTPUT_PATH from part 2. 

2. The WT_HEADER and STOCK_HEADER names must exactly match the names of those samples.

Options: There are three separate threshold calculations included. 

Threshold 'a' -- Lists all the possible barcodes that were counted in either the WT stocks or the barcode stocks.  This would include barcodes that were >0 in the barcode stocks, but =0 in the WT stocks.  Then, the mean frequency and standard deviation of all the barcodes that are not the WT barcode sequence is calculated from the WT stock sequence data.  Threshold 'a' equals this mean + 3x standard deviation.

Threshold 'b' -- Lists all the possible barcodes that were counted as >0 ONLY in the WT stocks.  Then, the mean frequency and standard deviation of all the barcodes that are not the WT barcode sequence is calculated from the WT stock sequence data.  Threshold 'b' equals this mean + 3x standard deviation.

Threshold 'c' -- Threshold 'c' (which we have chosen to use) uses the frequency of the most common barcode in the WT sample that was NOT the WT sequence.  When there are replicates of the WT stock, then threshold 'c' uses the highest value of these two replicates.  

